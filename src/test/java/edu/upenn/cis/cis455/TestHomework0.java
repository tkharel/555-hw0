package edu.upenn.cis.cis455;

import static org.junit.Assert.assertTrue;

import java.net.InetSocketAddress;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.sun.net.httpserver.HttpServer;


public class TestHomework0 {
    HttpServer server;
            
    @Before
    public void setUp() throws Exception {
        server = HttpServer.create(new InetSocketAddress(8888), 0);
        server.createContext("/test", new SimpleWebServer.MyHandler());
        server.setExecutor(null); // creates a default executor
        server.start();
    }
    
    @Test
    public void testResponse() throws Exception {
        assertTrue(true);
    }
    
    @After
    public void tearDown() throws Exception {
        server.stop(0);
    }
}
